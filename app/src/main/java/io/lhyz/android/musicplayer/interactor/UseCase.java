package io.lhyz.android.musicplayer.interactor;

import io.lhyz.android.musicplayer.executor.PostExecutionThread;
import io.lhyz.android.musicplayer.executor.ThreadExecutor;
import rx.Observable;
import rx.Subscription;
import rx.observers.Subscribers;
import rx.schedulers.Schedulers;

public abstract class UseCase {

    private PostExecutionThread mPostExecutionThread;
    private ThreadExecutor mThreadExecutor;

    private Subscription mSubscription = Subscribers.empty();

    public UseCase(PostExecutionThread postExecutionThread,
                   ThreadExecutor threadExecutor) {
        mPostExecutionThread = postExecutionThread;
        mThreadExecutor = threadExecutor;
    }

    protected abstract Observable buildObservable();

    @SuppressWarnings("unchecked")
    public void execute(DefaultSubscriber subscriber) {
        mSubscription = buildObservable()
                .subscribeOn(Schedulers.from(mThreadExecutor))
                .observeOn(mPostExecutionThread.getScheduler())
                .subscribe(subscriber);
    }

    public void unsubscribe() {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }

}

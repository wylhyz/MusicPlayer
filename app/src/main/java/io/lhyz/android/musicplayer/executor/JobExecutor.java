package io.lhyz.android.musicplayer.executor;

import android.support.annotation.NonNull;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.inject.Inject;

import io.lhyz.android.musicplayer.di.annotation.PerApplication;

/**
 * 工作线程（后台线程）
 */
@PerApplication
public class JobExecutor implements ThreadExecutor {

    private static final int CORE_POOL_SIZE = 3;
    private static final int MAX_POOL_SIZE = 5;
    private static final int KEEP_ALIVE_TIME = 120;
    private static final TimeUnit TIME_UNIT = TimeUnit.SECONDS;

    BlockingDeque<Runnable> mDeque = new LinkedBlockingDeque<>(5);
    ThreadFactory mThreadFactory = new ThreadFactory() {
        private final AtomicInteger mCount = new AtomicInteger(1);

        @Override
        public Thread newThread(@NonNull Runnable r) {
            return new Thread(r, "JobThread#" + mCount.getAndIncrement());
        }
    };

    ThreadPoolExecutor mThreadPoolExecutor;

    /**
     * 作为被provide的单例，构造参数是必须被inject注解的
     */
    @Inject
    public JobExecutor() {
        mThreadPoolExecutor = new ThreadPoolExecutor(
                CORE_POOL_SIZE,
                MAX_POOL_SIZE,
                KEEP_ALIVE_TIME,
                TIME_UNIT,
                mDeque,
                mThreadFactory);
    }

    @Override
    public void execute(@NonNull Runnable command) {
        mThreadPoolExecutor.execute(command);
    }

}

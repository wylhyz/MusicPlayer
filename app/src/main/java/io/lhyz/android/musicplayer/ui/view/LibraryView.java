package io.lhyz.android.musicplayer.ui.view;

import java.util.List;

import io.lhyz.android.musicplayer.bean.Music;
import io.lhyz.android.musicplayer.ui.LoadDataView;

public interface LibraryView extends LoadDataView {
    void showAllMusic(List<Music> musicList);
}

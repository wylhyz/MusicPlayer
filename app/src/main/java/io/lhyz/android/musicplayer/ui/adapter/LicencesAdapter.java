package io.lhyz.android.musicplayer.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.lhyz.android.musicplayer.R;

/**
 * 用来显示使用的开源项目地址的adapter(暂时没用)
 */
@SuppressWarnings("unused")
public class LicencesAdapter extends BaseAdapter {

    private static List<Item> mLicences = new ArrayList<>();

    static final class Item {
        int software_name;
        int licence_url;

        public Item(int software_name, int licence_url) {
            this.software_name = software_name;
            this.licence_url = licence_url;
        }
    }

    static {
        mLicences.add(new Item(R.string.software_exoplayer, R.string.licence_exoplayer));
        mLicences.add(new Item(R.string.software_junit, R.string.licence_junit));
        mLicences.add(new Item(R.string.software_greendao, R.string.licence_greendao));
        mLicences.add(new Item(R.string.software_rxjava, R.string.licence_rxjava));
        mLicences.add(new Item(R.string.software_rxandroid, R.string.licence_rxandroid));
        mLicences.add(new Item(R.string.software_butterknife, R.string.licence_butterknife));
        mLicences.add(new Item(R.string.software_dagger, R.string.licence_dagger));
    }

    @Override
    public int getCount() {
        return mLicences.size();
    }

    @Override
    public Item getItem(int position) {
        return mLicences.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.licence_item, parent, false);
            holder = new ViewHolder();
            holder.tvSoftware = (TextView) convertView.findViewById(R.id.tv_software);
            holder.tvLicence = (TextView) convertView.findViewById(R.id.tv_licence);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvSoftware.setText(mLicences.get(position).software_name);
        holder.tvLicence.setText(mLicences.get(position).licence_url);

        return convertView;
    }

    static class ViewHolder {
        TextView tvSoftware;
        TextView tvLicence;
    }
}

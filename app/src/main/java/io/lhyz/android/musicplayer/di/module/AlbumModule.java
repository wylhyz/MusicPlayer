package io.lhyz.android.musicplayer.di.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import io.lhyz.android.musicplayer.executor.PostExecutionThread;
import io.lhyz.android.musicplayer.executor.ThreadExecutor;
import io.lhyz.android.musicplayer.interactor.UseCase;
import io.lhyz.android.musicplayer.data.AlbumProvider;
import io.lhyz.android.musicplayer.di.annotation.AllAlbum;
import io.lhyz.android.musicplayer.di.annotation.OneAlbum;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import io.lhyz.android.musicplayer.interactor.GetAllAlbumUseCase;
import io.lhyz.android.musicplayer.interactor.GetOneAlbumUseCase;

@Module
public class AlbumModule {

    private long albumId;

    public AlbumModule(long albumId) {
        this.albumId = albumId;
    }

    @Provides
    @PerActivity
    AlbumProvider provideAlbum(Context context) {
        return new AlbumProvider(context);
    }

    @PerActivity
    @Provides
    @AllAlbum
    UseCase provideAllAlbumUseCase(GetAllAlbumUseCase useCase) {
        return useCase;
    }

    @Provides
    @PerActivity
    @OneAlbum
    UseCase provideOneAlbumUseCase(AlbumProvider provider,
                                   ThreadExecutor threadExecutor,
                                   PostExecutionThread postExecutionThread) {
        return new GetOneAlbumUseCase(albumId, provider, postExecutionThread, threadExecutor);
    }
}

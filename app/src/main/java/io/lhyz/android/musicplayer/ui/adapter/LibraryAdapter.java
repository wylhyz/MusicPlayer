package io.lhyz.android.musicplayer.ui.adapter;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.lhyz.android.musicplayer.R;
import io.lhyz.android.musicplayer.UAMP;
import io.lhyz.android.musicplayer.bean.Music;
import io.lhyz.android.musicplayer.di.component.DaggerApplicationComponent;
import io.lhyz.android.musicplayer.di.module.ApplicationModule;
import io.lhyz.android.musicplayer.event.MusicEventBus;
import io.lhyz.android.musicplayer.event.MusicSelectedEvent;
import io.lhyz.android.musicplayer.manager.ConfigureManager;
import io.lhyz.android.musicplayer.utils.ImageLoader;


public class LibraryAdapter extends RecyclerView.Adapter<LibraryAdapter.MainViewHolder> {

    private List<Music> mData;
    private int lastSelected = -1;
    private long currentMusicID = -1;//只保存当前播放的
    private WeakReference<Context> mContextWeakRef;
    private ImageLoader mImageLoader;

    @Inject
    ConfigureManager mConfigureManager;

    public LibraryAdapter(Context context) {
        mData = new ArrayList<>(10);
        mContextWeakRef = new WeakReference<>(context);

        DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(UAMP.getInstance()))
                .build()
                .inject(this);

        currentMusicID = mConfigureManager.getNowPlaying();//获取保存的播放对象（默认-1）

        mImageLoader = ImageLoader.build(UAMP.getInstance());
    }

    public void updateLibrary(List<Music> data) {
        if (mData.size() != 0) {
            mData.clear();
        }
        mData.addAll(data);
        //每次刷新都去找看上一次被播放的还存不存在，如果存在就置位
        if (currentMusicID != -1) {
            for (int i = 0; i < mData.size(); ++i) {
                if (mData.get(i).getId() == currentMusicID) {
                    lastSelected = i;
                    mData.get(i).setPlaying(true);
                    //每次也更新播放控制fragment（使用默认事件标志）
                    MusicEventBus.post(new MusicSelectedEvent(mData.get(i)));
                }
            }
        }
        notifyDataSetChanged();
    }

    /**
     * viewholder类似于原来的convertView（当已经存在的时候就不会被调到而是重用）
     */
    static final class MainViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.thumb_art)
        ImageView imgThumbArt;
        @BindView(R.id.title)
        TextView tvTitle;
        @BindView(R.id.artist)
        TextView tvArtist;
        @BindView(R.id.playing_status)
        ImageView imgPlayingStatus;

        @BindView(R.id.item_music)
        View rootView;

        public MainViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MainViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_music, parent, false));
    }

    @Override
    public void onBindViewHolder(final MainViewHolder holder, int position) {
        final int pos = holder.getAdapterPosition();
        final Music music = mData.get(pos);
        final ImageView imageView = holder.imgThumbArt;
        imageView.setImageResource(R.drawable.ic_launcher);//默认图片
        imageView.setTag(music.getId());//设置一个tag（第一次使用时候）

        if (imageView.getTag() != null && (long) imageView.getTag() == music.getId()) {
            mImageLoader.bindMusic(music.getId(), imageView);
        }

        holder.tvTitle.setText(music.getTitle());
        holder.tvArtist.setText(music.getArtist());

        if (music.isPlaying()) {
            holder.imgPlayingStatus.setVisibility(View.VISIBLE);
        } else {
            holder.imgPlayingStatus.setVisibility(View.GONE);
        }

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMusicID = music.getId();
                mConfigureManager.setNowPlaying(currentMusicID);

                if (lastSelected >= 0) {
                    mData.get(lastSelected).setPlaying(false);
                    notifyItemChanged(lastSelected);
                }
                lastSelected = pos;
                music.setPlaying(true);
                notifyItemChanged(pos);

                MusicEventBus.post(new MusicSelectedEvent(music, MusicSelectedEvent.MARK_TO_PLAYING));
            }
        });

        holder.rootView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new AlertDialog.Builder(mContextWeakRef.get())
                        .setMessage("你想干什么")
                        .setIcon(null).create().show();
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }
}

package io.lhyz.android.musicplayer.interactor;

import javax.inject.Inject;

import io.lhyz.android.musicplayer.executor.PostExecutionThread;
import io.lhyz.android.musicplayer.executor.ThreadExecutor;
import io.lhyz.android.musicplayer.data.MusicProvider;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import rx.Observable;

@PerActivity
public class GetAllMusicUseCase extends UseCase {

    MusicProvider mMusicProvider;

    @Inject
    public GetAllMusicUseCase(MusicProvider musicProvider,
                              PostExecutionThread postExecutionThread,
                              ThreadExecutor threadExecutor) {
        super(postExecutionThread, threadExecutor);

        mMusicProvider = musicProvider;
    }

    @Override
    protected Observable buildObservable() {
        return mMusicProvider.getAllMusic();
    }
}

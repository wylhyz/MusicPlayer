package io.lhyz.android.musicplayer.interactor;

import javax.inject.Inject;

import io.lhyz.android.musicplayer.executor.PostExecutionThread;
import io.lhyz.android.musicplayer.executor.ThreadExecutor;
import io.lhyz.android.musicplayer.data.ArtistProvider;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import rx.Observable;

@PerActivity
public class GetAllArtistUseCase extends UseCase {
    private ArtistProvider mArtistProvider;

    @Inject
    public GetAllArtistUseCase(ArtistProvider artistProvider,
                               PostExecutionThread postExecutionThread,
                               ThreadExecutor threadExecutor) {
        super(postExecutionThread, threadExecutor);

        mArtistProvider = artistProvider;
    }

    @Override
    protected Observable buildObservable() {
        return mArtistProvider.getAllArtist();
    }
}

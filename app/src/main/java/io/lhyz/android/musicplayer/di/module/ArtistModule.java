package io.lhyz.android.musicplayer.di.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import io.lhyz.android.musicplayer.executor.PostExecutionThread;
import io.lhyz.android.musicplayer.executor.ThreadExecutor;
import io.lhyz.android.musicplayer.interactor.UseCase;
import io.lhyz.android.musicplayer.data.ArtistProvider;
import io.lhyz.android.musicplayer.di.annotation.AllArtist;
import io.lhyz.android.musicplayer.di.annotation.OneArtist;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import io.lhyz.android.musicplayer.interactor.GetAllArtistUseCase;
import io.lhyz.android.musicplayer.interactor.GetOneArtistUseCase;

@Module
public class ArtistModule {

    private final long artistId;

    public ArtistModule(long artistId) {
        this.artistId = artistId;
    }

    @Provides
    @PerActivity
    ArtistProvider provideArtist(Context context) {
        return new ArtistProvider(context);
    }

    @PerActivity
    @Provides
    @AllArtist
    UseCase provideAllArtistUseCase(GetAllArtistUseCase useCase) {
        return useCase;
    }

    @PerActivity
    @Provides
    @OneArtist
    UseCase provideOneArtistUseCase(ArtistProvider artistProvider,
                                    PostExecutionThread postExecutionThread,
                                    ThreadExecutor threadExecutor) {
        return new GetOneArtistUseCase(artistId, artistProvider, postExecutionThread, threadExecutor);
    }
}

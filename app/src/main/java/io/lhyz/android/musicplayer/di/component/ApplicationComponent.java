package io.lhyz.android.musicplayer.di.component;

import android.content.Context;

import dagger.Component;
import io.lhyz.android.musicplayer.UAMP;
import io.lhyz.android.musicplayer.executor.PostExecutionThread;
import io.lhyz.android.musicplayer.executor.ThreadExecutor;
import io.lhyz.android.musicplayer.di.annotation.PerApplication;
import io.lhyz.android.musicplayer.di.module.ApplicationModule;
import io.lhyz.android.musicplayer.ui.activity.MainActivity;
import io.lhyz.android.musicplayer.ui.activity.NowPlayingActivity;
import io.lhyz.android.musicplayer.ui.activity.SettingActivity;
import io.lhyz.android.musicplayer.ui.adapter.LibraryAdapter;

@PerApplication
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(SettingActivity.SettingFragment fragment);

    void inject(UAMP application);

    void inject(MainActivity activity);

    void inject(NowPlayingActivity activity);

    void inject(LibraryAdapter adapter);

    //在此component被其他component依赖的时候，下面这些参数可能是必须的
    Context context();

    ThreadExecutor threadExecutor();

    PostExecutionThread postExecutionThread();
}

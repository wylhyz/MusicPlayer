package io.lhyz.android.musicplayer.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.lhyz.android.musicplayer.R;
import io.lhyz.android.musicplayer.base.BaseFragment;

public class NowFragment2 extends BaseFragment {

    public static NowFragment2 newInstance() {
        return new NowFragment2();
    }

    @Override
    protected View createView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_now_playing2, container, false);
    }

    @Override
    protected void initInjector() {

    }
}

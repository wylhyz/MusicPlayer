package io.lhyz.android.musicplayer.exception;

public class ArtistNotFoundException extends RuntimeException {

    public ArtistNotFoundException() {
        super();
    }

    public ArtistNotFoundException(String detailMessage) {
        super(detailMessage);
    }

    public ArtistNotFoundException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ArtistNotFoundException(Throwable throwable) {
        super(throwable);
    }
}

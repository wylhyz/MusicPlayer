package io.lhyz.android.musicplayer.interactor;

import javax.inject.Inject;

import io.lhyz.android.musicplayer.executor.PostExecutionThread;
import io.lhyz.android.musicplayer.executor.ThreadExecutor;
import io.lhyz.android.musicplayer.data.ArtistProvider;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import rx.Observable;

@PerActivity
public class GetOneArtistUseCase extends UseCase {

    private ArtistProvider mArtistProvider;
    private final long artistId;

    @Inject
    public GetOneArtistUseCase(final long id, ArtistProvider artistProvider,
                               PostExecutionThread postExecutionThread,
                               ThreadExecutor threadExecutor) {
        super(postExecutionThread, threadExecutor);

        mArtistProvider = artistProvider;
        artistId = id;
    }

    @Override
    protected Observable buildObservable() {
        return mArtistProvider.getArtist(artistId);
    }
}

package io.lhyz.android.musicplayer.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        initInjector();
        initVariables();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected abstract void initVariables();

    protected abstract void initInjector();

    @LayoutRes
    protected abstract int getLayoutId();
}

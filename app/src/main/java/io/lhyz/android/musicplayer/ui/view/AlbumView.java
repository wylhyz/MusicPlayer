package io.lhyz.android.musicplayer.ui.view;

import java.util.List;

import io.lhyz.android.musicplayer.bean.Album;
import io.lhyz.android.musicplayer.ui.LoadDataView;

public interface AlbumView extends LoadDataView {
    void showAllAlbum(List<Album> albumList);
}

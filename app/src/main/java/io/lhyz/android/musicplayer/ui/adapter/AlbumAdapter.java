package io.lhyz.android.musicplayer.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.lhyz.android.musicplayer.R;
import io.lhyz.android.musicplayer.UAMP;
import io.lhyz.android.musicplayer.bean.Album;
import io.lhyz.android.musicplayer.utils.ImageLoader;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder> {

    private List<Album> mData;
    private ImageLoader mImageLoader;

    public AlbumAdapter(Context context) {
        mData = new ArrayList<>(10);
        mImageLoader = ImageLoader.build(UAMP.getInstance());
    }

    public void updateAlbum(List<Album> data) {
        if (mData.size() != 0) {
            mData.clear();
        }
        mData.addAll(data);
        notifyDataSetChanged();
    }

    static final class AlbumViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.music_art)
        ImageView imgAlbumArt;
        @BindView(R.id.album_title)
        TextView tvTitle;
        @BindView(R.id.album_artist)
        TextView tvArtist;

        public AlbumViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public AlbumViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AlbumViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_album, parent, false));
    }

    @Override
    public void onBindViewHolder(AlbumViewHolder holder, int position) {
        final int pos = holder.getAdapterPosition();
        final Album album = mData.get(pos);
        final ImageView imageView = holder.imgAlbumArt;
        imageView.setImageResource(R.drawable.ic_launcher);
        imageView.setTag(album.getId());

        if (imageView.getTag() != null && (long) imageView.getTag() == album.getId()) {
            mImageLoader.bindAlbum(album.getId(), imageView);
        }

        holder.tvTitle.setText(album.getTitle());
        holder.tvArtist.setText(album.getArtist());
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }
}

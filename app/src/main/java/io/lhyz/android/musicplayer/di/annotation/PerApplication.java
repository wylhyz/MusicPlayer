package io.lhyz.android.musicplayer.di.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * 全局application作用域
 */
@Scope
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface PerApplication {
}

package io.lhyz.android.musicplayer.ui.fragment;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import io.lhyz.android.musicplayer.IMusicPlaybackManager;
import io.lhyz.android.musicplayer.R;
import io.lhyz.android.musicplayer.UAMP;
import io.lhyz.android.musicplayer.base.BaseFragment;
import io.lhyz.android.musicplayer.bean.Music;
import io.lhyz.android.musicplayer.event.MusicEventBus;
import io.lhyz.android.musicplayer.event.MusicSelectedEvent;
import io.lhyz.android.musicplayer.navigation.Navigator;
import io.lhyz.android.musicplayer.service.MusicPlayback;
import io.lhyz.android.musicplayer.utils.ImageLoader;

/**
 * 快捷控制播放栏目，此栏目与service绑定，同时不轻易解绑
 */
public class QuickControlFragment extends BaseFragment {
    @BindView(R.id.fragment_quick_control)
    View mRootView;
    @BindView(R.id.play_pause)
    ImageButton mPlayPauseButton;
    @BindView(R.id.music_art)
    ImageView imgMusicArt;
    @BindView(R.id.title)
    TextView tvTitle;
    @BindView(R.id.artist)
    TextView tvArtist;

    @BindView(R.id.progress)
    ProgressBar mProgressBar;

    private boolean isPlaying = false;
    private Music mCurrentMusic;

    private ImageLoader mImageLoader;

    /**
     * 与服务相关的对象
     */
    private IMusicPlaybackManager mPlaybackManager;//用来与服务交互的对象
    private MusicPlayback.ServiceToken mToken;//此token虽然在fragment中，但是实际上是与activity绑定的

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mPlaybackManager = IMusicPlaybackManager.Stub.asInterface(service);
            //因为这里的初始化总是可能在EventBus之后，所以，很有可能因为某些原因此处的管理器还没有初始化，所以需要通过重发事件来初始化播放器
            MusicEventBus.post(new MusicSelectedEvent(mCurrentMusic, MusicSelectedEvent.MARK_TO_DISPLAY));
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    public static QuickControlFragment newInstance() {
        return new QuickControlFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mToken = MusicPlayback.bindService(getActivity(), mServiceConnection);
        MusicEventBus.register(this);
        mImageLoader = ImageLoader.build(UAMP.getInstance());
    }

    @Override
    public void onDestroy() {
        MusicEventBus.unregister(this);
        MusicPlayback.unbindService(mToken);
        mToken = null;
        super.onDestroy();
    }

    @Override
    protected View createView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_quick_control, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigator.navigateToNowPlayingActivity(getActivity());
            }
        });

        mPlayPauseButton.setOnClickListener(new View.OnClickListener() {//根据播放与否操作
            @Override
            public void onClick(View v) {
                //这里只监听对播放管理器的更改,然后可以将标志置位(但是对此按钮状态的更新还需要管理器状态)
                if (isPlaying) {
                    try {
                        mPlaybackManager.pause();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        mPlaybackManager.play();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                updatePlayPause();
            }
        });

        mRootView.setEnabled(false);
        mPlayPauseButton.setEnabled(false);//初始化的时候是不允许点击事件的
    }

    public void updatePlayPause() {
        try {
            if (mPlaybackManager.isPlaying()) {
                mPlayPauseButton.setImageResource(R.drawable.ic_pause_black_36dp);
                isPlaying = true;
            } else {
                mPlayPauseButton.setImageResource(R.drawable.ic_play_arrow_black_36dp);
                isPlaying = false;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void initInjector() {

    }

    /**
     * 被subscribe的方法必须是public的，
     * 此方法由外部组件使用事件总线调用
     *
     * @param event 收到的包含music对象的事件
     */
    @Subscribe
    @SuppressWarnings("unused")
    public void updateMusic(MusicSelectedEvent event) {
        if (event.music == null) {
            return;
        }
        mCurrentMusic = event.music;
        tvTitle.setText(event.music.getTitle());
        tvArtist.setText(event.music.getArtist());
        mImageLoader.bindMusic(event.music.getId(), imgMusicArt);

        if (mCurrentMusic != null && !mPlayPauseButton.isEnabled()) {
            mPlayPauseButton.setEnabled(true);//一旦当前有歌曲被选中，则可以监听点击事件
            mRootView.setEnabled(true);
        }
        //启动的时候恢复播放文件专改，但是服务还没有初始化完成，所以播放器还无法初始化，所以要在上面的connection中初始化完毕之后再更新一次状态
        if (mPlaybackManager == null) {
            return;
        }

        try {
            if (!mCurrentMusic.getPath().equals(mPlaybackManager.getPath())) {
                mPlaybackManager.stop();
                mPlaybackManager.prepare(mCurrentMusic.getPath());
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        try {
            if (event.mark == MusicSelectedEvent.MARK_TO_PLAYING) {
                mPlaybackManager.play();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        updatePlayPause();
    }
}

package io.lhyz.android.musicplayer.ui.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Configuration;
import android.os.Handler;
import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import javax.inject.Inject;

import butterknife.BindView;
import io.lhyz.android.musicplayer.AppInfo;
import io.lhyz.android.musicplayer.UAMP;
import io.lhyz.android.musicplayer.R;
import io.lhyz.android.musicplayer.base.BaseActivity;
import io.lhyz.android.musicplayer.utils.ActivityUtil;
import io.lhyz.android.musicplayer.di.component.DaggerApplicationComponent;
import io.lhyz.android.musicplayer.di.module.ApplicationModule;
import io.lhyz.android.musicplayer.manager.ConfigureManager;
import io.lhyz.android.musicplayer.navigation.Navigator;
import io.lhyz.android.musicplayer.ui.fragment.AlbumFragment;
import io.lhyz.android.musicplayer.ui.fragment.ArtistFragment;
import io.lhyz.android.musicplayer.ui.fragment.LibraryFragment;
import io.lhyz.android.musicplayer.ui.fragment.QuickControlFragment;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by lhyz on 2016/3/13.
 * 主播放界面
 */
public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.nav_view)
    NavigationView mNavigationView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    FragmentManager mFragmentManager;

    LibraryFragment mLibraryFragment;
    AlbumFragment mAlbumFragment;
    ArtistFragment mArtistFragment;

    @Inject
    ConfigureManager mConfigureManager;

    private int uiMode = -1;
    private Handler mHandler;

    @Override
    protected void initInjector() {//初始化依赖注入
        DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(UAMP.getInstance()))
                .build()
                .inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /**
         * 由于对于夜间模式的判断只有在这个方法中比较好，但是直接在下面（needReCreate = true;）这两行调用recreate的话会报
         * java.lang.RuntimeException: Performing pause of activity that is not resumed: 异常
         * 是由于在这里调用重建的话，onResume方法尚未完成，也就是说尚未完成到resume状态的转换，因此报错。
         *
         * 解决方法就是使用handler post延迟发送一个事件到当前ui线程（延迟为0表示直接将消息加到消息队列尾部），等待当前resume完成
         *
         * 方案来自StackOverflow http://stackoverflow.com/questions/10844112/runtimeexception-performing-pause-of-activity-that-is-not-resumed
         */

        boolean needReCreate = false;
        //由于当前activity是singleTask，无论如何跳转，回到此activity时此方法必定被调用
        //因此在这里处理夜间模式的相关逻辑
        if (mConfigureManager.isNightMode()) {//判断设置是夜间模式
            if (uiMode != Configuration.UI_MODE_NIGHT_YES) {//那么如果当不是夜间模式，直接重建
                needReCreate = true;
            }
        } else {//判断设置不是夜间模式
            if (uiMode != Configuration.UI_MODE_NIGHT_NO) {//那么如果当前是夜间模式，直接重建
                needReCreate = true;
            }
        }

        final boolean mark = needReCreate;
        final Activity activity = this;
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mark) {
                    activity.recreate();
                }
            }
        }, 0);

        //通过读取配置文件选取当前显示的fragment
        showActiveFragment();
        showQuickControlFragment();
    }

    private void showQuickControlFragment() {
        Fragment fragment = mFragmentManager.findFragmentById(R.id.quick_control);
        if (fragment == null) {
            fragment = QuickControlFragment.newInstance();
            mFragmentManager.beginTransaction()
                    .add(R.id.quick_control, fragment)
                    .commit();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initVariables() {
        //这个是获取当前ui模式的（根据结果可以判断当前是否是夜间模式）
        uiMode = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;

        mHandler = new Handler();

        mFragmentManager = getSupportFragmentManager();
        setSupportActionBar(mToolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                mToolbar, R.string.open_content_drawer, R.string.close_content_drawer);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView.setNavigationItemSelectedListener(this);

        mLibraryFragment = LibraryFragment.newInstance();
        mAlbumFragment = AlbumFragment.newInstance();
        mArtistFragment = ArtistFragment.newInstance();


        ActivityUtil.addFragmentToActivity(mFragmentManager, mLibraryFragment, R.id.main_container);
        ActivityUtil.addFragmentToActivity(mFragmentManager, mAlbumFragment, R.id.main_container);
        ActivityUtil.addFragmentToActivity(mFragmentManager, mArtistFragment, R.id.main_container);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void setActionBarTitle(@StringRes int title) {
        checkNotNull(getSupportActionBar()).setTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_filter) {
            showFilterPopupMenu();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showFilterPopupMenu() {
        PopupMenu popupMenu = new PopupMenu(this, findViewById(R.id.action_filter));
        popupMenu.getMenuInflater().inflate(R.menu.menu_filter, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.all:
                        mConfigureManager.setActiveTab(ConfigureManager.TAB_LIBRARY);
                        break;
                    case R.id.album:
                        mConfigureManager.setActiveTab(ConfigureManager.TAB_ALBUM);
                        break;
                    case R.id.artist:
                        mConfigureManager.setActiveTab(ConfigureManager.TAB_ARTIST);
                        break;
                }
                showActiveFragment();
                return true;
            }
        });

        popupMenu.show();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_allmusic:
                return true;
            case R.id.nav_playlist:
                return true;
            case R.id.nav_playqueue:
                return true;

            case R.id.nav_nowpaying:
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                }
                Navigator.navigateToNowPlayingActivity(this);
                return true;
            case R.id.nav_setting:
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                }
                Navigator.navigateToSettingActivity(this);
                return true;
            case R.id.nav_info:
                showInfoDialog();
                return true;
        }
        return false;
    }

    public void showInfoDialog() {
        Dialog dialog = new AlertDialog.Builder(this)
                .setTitle(String.format(getString(R.string.title_app_info), AppInfo.getAppName(), AppInfo.getVersionName()))
                .setIcon(null)
                .setMessage(R.string.info)
                .setPositiveButton("确认", null)
                .create();
        dialog.show();
    }

    private void showActiveFragment() {
        switch (mConfigureManager.getActiveTab()) {
            case ConfigureManager.TAB_LIBRARY:
                showLibraryFragment();
                break;
            case ConfigureManager.TAB_ALBUM:
                showAlbumFragment();
                break;
            case ConfigureManager.TAB_ARTIST:
                showArtistFragment();
                break;
        }
    }

    private void showLibraryFragment() {
        setActionBarTitle(R.string.title_all_music);
        mFragmentManager.beginTransaction()
                .show(mLibraryFragment)
                .hide(mAlbumFragment)
                .hide(mArtistFragment)
                .commit();

    }

    private void showAlbumFragment() {
        setActionBarTitle(R.string.title_album);
        mFragmentManager.beginTransaction()
                .show(mAlbumFragment)
                .hide(mLibraryFragment)
                .hide(mArtistFragment)
                .commit();
    }

    private void showArtistFragment() {
        setActionBarTitle(R.string.title_artist);
        mFragmentManager.beginTransaction()
                .show(mArtistFragment)
                .hide(mLibraryFragment)
                .hide(mAlbumFragment)
                .commit();
    }
}


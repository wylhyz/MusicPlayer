package io.lhyz.android.musicplayer.executor;

import rx.Scheduler;

/**
 * 主线程，使用这个接口来获取rxjava使用的scheduler调度器
 */
public interface PostExecutionThread {
    Scheduler getScheduler();
}


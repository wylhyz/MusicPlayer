package io.lhyz.android.musicplayer.di.component;

import dagger.Component;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import io.lhyz.android.musicplayer.di.module.MusicModule;
import io.lhyz.android.musicplayer.ui.fragment.LibraryFragment;
import io.lhyz.android.musicplayer.ui.fragment.NowFragment1;

@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = MusicModule.class)
public interface MusicComponent {
    void inject(LibraryFragment fragment);
    void inject(NowFragment1 fragment);
}

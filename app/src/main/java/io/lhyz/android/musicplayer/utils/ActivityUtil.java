package io.lhyz.android.musicplayer.utils;

import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import io.lhyz.android.musicplayer.exception.InstantiateNotAllowException;

import static com.google.common.base.Preconditions.checkNotNull;

public final class ActivityUtil {

    private ActivityUtil() {
        throw new InstantiateNotAllowException();
    }

    public static void addFragmentToActivity(FragmentManager fragmentManager,
                                             Fragment fragment, @IdRes int containerId) {
        checkNotNull(fragmentManager);
        checkNotNull(fragment);

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(containerId, fragment);
        transaction.commit();
    }
}

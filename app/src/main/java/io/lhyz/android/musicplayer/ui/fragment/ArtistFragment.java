package io.lhyz.android.musicplayer.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import io.lhyz.android.musicplayer.UAMP;
import io.lhyz.android.musicplayer.R;
import io.lhyz.android.musicplayer.bean.Artist;
import io.lhyz.android.musicplayer.base.BaseFragment;
import io.lhyz.android.musicplayer.di.component.DaggerArtistComponent;
import io.lhyz.android.musicplayer.di.module.ArtistModule;
import io.lhyz.android.musicplayer.presenter.ArtistPresenter;
import io.lhyz.android.musicplayer.ui.adapter.ArtistAdapter;
import io.lhyz.android.musicplayer.ui.adapter.ArtistLayoutManager;
import io.lhyz.android.musicplayer.ui.view.ArtistView;

/**
 * 按照歌手分类显示歌曲
 */
public class ArtistFragment extends BaseFragment implements ArtistView {
    @BindView(R.id.spl_list)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.rv_list)
    RecyclerView mRecyclerView;

    @Inject
    ArtistPresenter mPresenter;

    ArtistAdapter mAdapter;

    public static ArtistFragment newInstance() {
        return new ArtistFragment();
    }

    @Override
    protected void initInjector() {
        DaggerArtistComponent.builder()
                .applicationComponent(((UAMP) getActivity().getApplication()).getApplicationComponent())
                .artistModule(new ArtistModule(0))
                .build()
                .inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.destroy();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSwipeRefreshLayout.setColorSchemeColors(R.color.colorPrimary, R.color.colorAccent, R.color.colorPrimaryDark);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.loadAllArtist();
            }
        });
        mAdapter = new ArtistAdapter();
        mRecyclerView.setLayoutManager(new ArtistLayoutManager(context()));
        mRecyclerView.setAdapter(mAdapter);
        mPresenter.setView(this);
        if (savedInstanceState == null) {
            mPresenter.loadAllArtist();
        }
    }

    @Override
    public void showAllArtist(List<Artist> artistList) {
        mAdapter.updateArtist(artistList);
    }

    @Override
    protected View createView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void showLoading() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public Context context() {
        return getActivity();
    }
}

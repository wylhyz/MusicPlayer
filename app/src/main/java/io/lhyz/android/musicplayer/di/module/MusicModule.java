package io.lhyz.android.musicplayer.di.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import io.lhyz.android.musicplayer.executor.PostExecutionThread;
import io.lhyz.android.musicplayer.executor.ThreadExecutor;
import io.lhyz.android.musicplayer.interactor.UseCase;
import io.lhyz.android.musicplayer.data.MusicProvider;
import io.lhyz.android.musicplayer.di.annotation.AllMusic;
import io.lhyz.android.musicplayer.di.annotation.OneMusic;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import io.lhyz.android.musicplayer.interactor.GetAllMusicUseCase;
import io.lhyz.android.musicplayer.interactor.GetOneMusicUseCase;

/**
 * 提供查询music的依赖接口
 */
@Module
public class MusicModule {

    private long musicId;

    public MusicModule(long musicId) {
        this.musicId = musicId;
    }

    @Provides
    @PerActivity
    MusicProvider provideMusic(Context context) {
        return new MusicProvider(context);
    }

    @Provides
    @PerActivity
    @AllMusic
    UseCase provideAllMusicUseCase(GetAllMusicUseCase useCase) {
        return useCase;
    }

    @Provides
    @PerActivity
    @OneMusic
    UseCase provideOneMusicUseCase(MusicProvider provider,
                                   ThreadExecutor threadExecutor,
                                   PostExecutionThread postExecutionThread) {
        return new GetOneMusicUseCase(musicId, provider, postExecutionThread, threadExecutor);
    }
}

package io.lhyz.android.musicplayer.ui.view;

import java.util.List;

import io.lhyz.android.musicplayer.bean.Artist;
import io.lhyz.android.musicplayer.ui.LoadDataView;

public interface ArtistView extends LoadDataView {
    void showAllArtist(List<Artist> artistList);
}

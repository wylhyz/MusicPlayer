package io.lhyz.android.musicplayer.ui.activity;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import javax.inject.Inject;

import butterknife.BindView;
import io.lhyz.android.musicplayer.UAMP;
import io.lhyz.android.musicplayer.R;
import io.lhyz.android.musicplayer.base.BaseActivity;
import io.lhyz.android.musicplayer.di.component.DaggerApplicationComponent;
import io.lhyz.android.musicplayer.di.module.ApplicationModule;
import io.lhyz.android.musicplayer.manager.ConfigureManager;
import io.lhyz.android.musicplayer.ui.fragment.NowFragment1;
import io.lhyz.android.musicplayer.ui.fragment.NowFragment2;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * 全屏播放界面（当前播放的歌曲）
 */
public class NowPlayingActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Inject
    ConfigureManager mConfigureManager;

    private FragmentManager mFragmentManager;

    @Override
    protected void initInjector() {
        DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(UAMP.getInstance()))
                .build()
                .inject(this);
    }

    @Override
    protected void initVariables() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = checkNotNull(getSupportActionBar());
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        mFragmentManager = getSupportFragmentManager();

        setNowThemeFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_now_playing;
    }

    private void setNowThemeFragment() {
        switch (mConfigureManager.getNowTheme()) {
            case ConfigureManager.NOW_THEME_1:
                mFragmentManager.beginTransaction()
                        .add(R.id.playing_container, NowFragment1.newInstance(mConfigureManager.getNowPlaying()))
                        .commit();
                break;
            case ConfigureManager.NOW_THEME_2:
                mFragmentManager.beginTransaction()
                        .add(R.id.playing_container, NowFragment2.newInstance())
                        .commit();
                break;
        }
    }
}

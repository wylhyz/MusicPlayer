package io.lhyz.android.musicplayer.ui.fragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import io.lhyz.android.musicplayer.UAMP;
import io.lhyz.android.musicplayer.R;
import io.lhyz.android.musicplayer.bean.Music;
import io.lhyz.android.musicplayer.base.BaseFragment;
import io.lhyz.android.musicplayer.di.component.DaggerMusicComponent;
import io.lhyz.android.musicplayer.di.module.MusicModule;
import io.lhyz.android.musicplayer.presenter.LibraryPresenter;
import io.lhyz.android.musicplayer.ui.adapter.LibraryAdapter;
import io.lhyz.android.musicplayer.ui.adapter.LibraryLayoutManager;
import io.lhyz.android.musicplayer.ui.view.LibraryView;

/**
 * 所有歌曲界面，不分类(音乐库)
 * <p>
 * 这个类是我尝试重新Clean架构之后的第一个模板代码。
 * <p>
 * 作为View层的fragment，不能包含任何业务代码，只需要实现关于UI的相关方法，然后再其生命周期中调用presenter的相应生命周期
 */
public class LibraryFragment extends BaseFragment
        implements LibraryView {

    @BindView(R.id.spl_list)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.rv_list)
    RecyclerView mRecyclerView;

    @Inject
    LibraryPresenter mPresenter;

    LibraryAdapter mAdapter;

    public static LibraryFragment newInstance() {
        return new LibraryFragment();
    }

    @Override
    protected void initInjector() {
        DaggerMusicComponent.builder()
                .applicationComponent(((UAMP) getActivity().getApplication()).getApplicationComponent())
                .musicModule(new MusicModule(0))
                .build()
                .inject(this);
    }

    @Override
    protected View createView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSwipeRefreshLayout.setColorSchemeColors(R.color.colorPrimary, R.color.colorAccent, R.color.colorPrimaryDark);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.loadAllMusic();
            }
        });
        mAdapter = new LibraryAdapter(context());
        mRecyclerView.setLayoutManager(new LibraryLayoutManager(context()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(context(), getActivity().getResources().getDisplayMetrics().density));
        mRecyclerView.setAdapter(mAdapter);

        mPresenter.setView(this);
        if (savedInstanceState == null) {
            mPresenter.loadAllMusic();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.destroy();
    }

    @Override
    public void showAllMusic(List<Music> musics) {
        mAdapter.updateLibrary(musics);
    }

    @Override
    public void showLoading() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(context(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public Context context() {
        return getContext();
    }


    static class DividerItemDecoration extends RecyclerView.ItemDecoration {
        private static final int[] ATTRS = new int[]{android.R.attr.listDivider};
        private final Drawable mDivider;
        private float density;

        /**
         * Default divider will be used
         */
        public DividerItemDecoration(Context context, float density) {
            final TypedArray styledAttributes = context.obtainStyledAttributes(ATTRS);
            mDivider = styledAttributes.getDrawable(0);
            styledAttributes.recycle();

            this.density = density;
        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = parent.getPaddingLeft() + (int) (12 * density);
            int right = parent.getWidth() - parent.getPaddingRight() - (int) (12 * density);

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + mDivider.getIntrinsicHeight();

                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }

}

package io.lhyz.android.musicplayer.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class Music implements Parcelable {

    private Long id;
    private String title;
    private String artist;
    private String path;

    private boolean isPlaying = false;//是否被选中/播放的标签（标记知识正在播放，但是不作为播放控制的标记）

    public Music(Parcel in) {
        id = in.readLong();
        title = in.readString();
        artist = in.readString();
        path = in.readString();
        isPlaying = in.readByte() != 0;
    }

    public Music(Long id, String title, String artist, String path) {
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.path = path;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeString(artist);
        dest.writeString(path);
        dest.writeByte((byte) (isPlaying ? 1 : 0));
    }

    public static final Parcelable.Creator<Music> CREATOR = new Creator<Music>() {
        @Override
        public Music createFromParcel(Parcel source) {
            return new Music(source);
        }

        @Override
        public Music[] newArray(int size) {
            return new Music[size];
        }
    };
}

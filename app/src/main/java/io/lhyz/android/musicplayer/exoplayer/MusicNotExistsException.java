package io.lhyz.android.musicplayer.exoplayer;

public class MusicNotExistsException extends RuntimeException {

    public MusicNotExistsException() {
        super();
    }

    public MusicNotExistsException(String detailMessage) {
        super(detailMessage);
    }

    public MusicNotExistsException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public MusicNotExistsException(Throwable throwable) {
        super(throwable);
    }
}

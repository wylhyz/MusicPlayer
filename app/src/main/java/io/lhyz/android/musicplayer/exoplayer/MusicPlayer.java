package io.lhyz.android.musicplayer.exoplayer;

import android.content.Context;
import android.net.Uri;

import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.MediaCodecSelector;
import com.google.android.exoplayer.SampleSource;
import com.google.android.exoplayer.extractor.ExtractorSampleSource;
import com.google.android.exoplayer.upstream.Allocator;
import com.google.android.exoplayer.upstream.DataSource;
import com.google.android.exoplayer.upstream.DefaultAllocator;
import com.google.android.exoplayer.upstream.DefaultUriDataSource;
import com.google.android.exoplayer.util.PlayerControl;
import com.google.android.exoplayer.util.Util;

import java.io.File;

/**
 * STATE_IDLE = 1;//空闲状态，player既没有初始化也没有设置好track
 * <p/>
 * STATE_PREPARING = 2;//正在prepare
 * <p/>
 * STATE_BUFFERING = 3;//已经prepare，但是由于数据来源较慢，所以仍在缓冲
 * <p/>
 * STATE_READY = 4;//准备好，随时可以从当前位置播放，（当然只是getPlayWhenReady返回true的话）
 * <p/>
 * STATE_ENDED = 5;//播放完成
 * <p/>
 * TRACK_DISABLED = -1;
 * <p/>
 * TRACK_DEFAULT = 0;
 * <p/>
 * UNKNOWN_TIME = -1;
 */

/**
 * 封装了ExoPlayer和其PlayControl。暴露了对外接口
 */
public class MusicPlayer {

    private static final int BUFFER_SEGMENT_SIZE = 64 * 1024;
    private static final int BUFFER_SEGMENT_COUNT = 256;
    private static final int RENDERER_COUNT = 1;

    private Allocator mAllocator;
    private DataSource mDataSource;
    private ExoPlayer mExoPlayer;
    private PlayerControl mPlayerControl;

    private String mCurrentPath = null;//当前播放的path

    //创建播放器需要初始的化对象
    public MusicPlayer(Context context) {
        mAllocator = new DefaultAllocator(BUFFER_SEGMENT_SIZE);
        mDataSource = new DefaultUriDataSource(context, Util.getUserAgent(context, "MusicPlayer"));
        mExoPlayer = ExoPlayer.Factory.newInstance(RENDERER_COUNT);
        mPlayerControl = new PlayerControl(mExoPlayer);
    }

    //每次播放一个新曲目的时候需要的准备工作
    public void prepare(String path) {
        File file = new File(path);
        if (!file.exists()) {
            //音乐不存在 直接抛出异常，此刻不用更改状态，直接release即可
            throw new MusicNotExistsException();
        }
        mCurrentPath = path;

        Uri uri = Uri.fromFile(file);
        SampleSource sampleSource = new ExtractorSampleSource(
                uri, mDataSource,
                mAllocator,
                BUFFER_SEGMENT_SIZE * BUFFER_SEGMENT_COUNT
        );
        MediaCodecAudioTrackRenderer audioTrackRenderer =
                new MediaCodecAudioTrackRenderer(sampleSource, MediaCodecSelector.DEFAULT);

        mExoPlayer.prepare(audioTrackRenderer);
    }

    //开始播放
    public void play() {
        mPlayerControl.start();
    }

    //暂停播放
    public void pause() {
        mPlayerControl.pause();
    }

    //停止播放
    public void stop() {
        pause();//先暂停播放器，然后将播放位置置为0
        seekTo(0);
    }

    //重置播放位置
    public void seekTo(int time) {
        mPlayerControl.seekTo(time);
    }

    //是否正在播放
    public boolean isPlaying() {
        return mPlayerControl.isPlaying();
    }

    //获取音乐的长度
    public int getDuration() {
        return mPlayerControl.getDuration();
    }

    //获取播放到的位置
    public int getPosition() {
        return mPlayerControl.getCurrentPosition();
    }

    public String getPath() {
        return mCurrentPath;
    }

    //销毁播放器，就是将所有不需要的对象置为空
    public void release() {
        mPlayerControl = null;
        mAllocator = null;
        mDataSource = null;
        mExoPlayer.release();
        mExoPlayer = null;
    }
}

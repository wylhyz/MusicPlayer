package io.lhyz.android.musicplayer.bean;

public class Album implements java.io.Serializable {

    private Long id;
    private String artist;
    private Integer count;
    private String title;

    public Album() {
    }

    public Album(Long id) {
        this.id = id;
    }

    public Album(Long id, String artist, Integer count, String title) {
        this.id = id;
        this.artist = artist;
        this.count = count;
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}

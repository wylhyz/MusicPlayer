package io.lhyz.android.musicplayer.ui.fragment;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import io.lhyz.android.musicplayer.IMusicPlaybackManager;
import io.lhyz.android.musicplayer.R;
import io.lhyz.android.musicplayer.UAMP;
import io.lhyz.android.musicplayer.bean.Music;
import io.lhyz.android.musicplayer.di.annotation.OneMusic;
import io.lhyz.android.musicplayer.di.component.DaggerMusicComponent;
import io.lhyz.android.musicplayer.di.module.MusicModule;
import io.lhyz.android.musicplayer.interactor.DefaultSubscriber;
import io.lhyz.android.musicplayer.interactor.UseCase;
import io.lhyz.android.musicplayer.service.MusicPlayback;

import static com.google.common.base.Preconditions.checkNotNull;

public class NowFragment1 extends BaseNowFragment {

    private Music mCurrentMusic;
    private MusicPlayback.ServiceToken mToken;
    private IMusicPlaybackManager mMusicPlaybackManager;

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mMusicPlaybackManager = IMusicPlaybackManager.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    @Inject
    @OneMusic
    UseCase mUseCase;

    public static NowFragment1 newInstance(long musicId) {
        Bundle args = new Bundle();
        args.putLong(EXTRA_MUSIC_ID, musicId);
        NowFragment1 fragment = new NowFragment1();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUseCase.execute(new DefaultSubscriber<Music>() {
            @Override
            public void onSuccess(Music music) {
                mCurrentMusic = music;
                ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
                checkNotNull(actionBar).setTitle(music.getTitle());
                checkNotNull(actionBar).setSubtitle(music.getArtist());
            }
        });

        mToken = MusicPlayback.bindService(getActivity(), mServiceConnection);
    }

    @Override
    public void onDestroy() {
        MusicPlayback.unbindService(mToken);
        mToken = null;
        super.onDestroy();
    }

    @Override
    protected View createView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_now_playing1, container, false);
    }

    @Override
    protected void initInjector() {
        //由于初始化注入是最早的，暂时我又需要使用onCreate获取的Id，所以我在这里获取iD
        long id = getArguments().getLong(EXTRA_MUSIC_ID, -1);
        DaggerMusicComponent.builder()
                .applicationComponent(((UAMP) getActivity().getApplication()).getApplicationComponent())
                .musicModule(new MusicModule(id))
                .build()
                .inject(this);
    }
}

package io.lhyz.android.musicplayer.ui.adapter;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;

public class AlbumLayoutManager extends GridLayoutManager {
    //专辑总是比歌手多
    public AlbumLayoutManager(Context context) {
        super(context, 3);
    }
}

package io.lhyz.android.musicplayer.ui.fragment;

import android.support.v4.app.Fragment;

/**
 * 播放列表界面，提供给用户最近播放，最近喜爱的几首，最近添加的几个默认列表（后期考虑还可以由用户自己添加）
 */
public class PlaylistFragment extends Fragment {

}

package io.lhyz.android.musicplayer.interactor;

import javax.inject.Inject;

import io.lhyz.android.musicplayer.executor.PostExecutionThread;
import io.lhyz.android.musicplayer.executor.ThreadExecutor;
import io.lhyz.android.musicplayer.data.AlbumProvider;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import rx.Observable;

@PerActivity
public class GetAllAlbumUseCase extends UseCase {

    private AlbumProvider mAlbumProvider;

    @Inject
    public GetAllAlbumUseCase(AlbumProvider albumProvider,
                              PostExecutionThread postExecutionThread,
                              ThreadExecutor threadExecutor) {
        super(postExecutionThread, threadExecutor);

        mAlbumProvider = albumProvider;
    }

    @Override
    protected Observable buildObservable() {
        return mAlbumProvider.getAllAlbum();
    }
}

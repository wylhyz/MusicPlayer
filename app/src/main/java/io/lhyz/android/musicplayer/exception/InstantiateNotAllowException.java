package io.lhyz.android.musicplayer.exception;

public class InstantiateNotAllowException extends RuntimeException {
    public InstantiateNotAllowException() {
        super();
    }

    public InstantiateNotAllowException(String detailMessage) {
        super(detailMessage);
    }

    public InstantiateNotAllowException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public InstantiateNotAllowException(Throwable throwable) {
        super(throwable);
    }
}

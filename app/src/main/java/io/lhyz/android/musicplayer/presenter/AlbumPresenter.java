package io.lhyz.android.musicplayer.presenter;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import io.lhyz.android.musicplayer.bean.Album;
import io.lhyz.android.musicplayer.interactor.DefaultSubscriber;
import io.lhyz.android.musicplayer.interactor.UseCase;
import io.lhyz.android.musicplayer.di.annotation.AllAlbum;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import io.lhyz.android.musicplayer.ui.view.AlbumView;

@PerActivity
public class AlbumPresenter implements Presenter {

    AlbumView mAlbumView;

    UseCase mAllAlbumUseCase;

    @Inject
    public AlbumPresenter(@AllAlbum UseCase allAlbumUseCase) {
        mAllAlbumUseCase = allAlbumUseCase;
    }

    public void setView(@NonNull AlbumView albumView) {
        this.mAlbumView = albumView;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        mAllAlbumUseCase.unsubscribe();
        mAlbumView = null;
    }

    public void loadAllAlbum() {
        mAlbumView.showLoading();
        getAllAlbum();
    }

    private void getAllAlbum() {
        mAllAlbumUseCase.execute(new AllAlbumSubscriber());
    }


    private final class AllAlbumSubscriber extends DefaultSubscriber<List<Album>> {
        @Override
        public void onSuccess(List<Album> albumList) {
            mAlbumView.showAllAlbum(albumList);
            mAlbumView.hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            mAlbumView.hideLoading();
            mAlbumView.showError(e.getMessage());
        }
    }
}

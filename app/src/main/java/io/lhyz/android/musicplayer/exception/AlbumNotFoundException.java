package io.lhyz.android.musicplayer.exception;

public class AlbumNotFoundException extends RuntimeException {

    public AlbumNotFoundException() {
        super();
    }

    public AlbumNotFoundException(String detailMessage) {
        super(detailMessage);
    }

    public AlbumNotFoundException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public AlbumNotFoundException(Throwable throwable) {
        super(throwable);
    }
}

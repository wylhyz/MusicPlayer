package io.lhyz.android.musicplayer.log;

import android.util.Log;

/**
 * log 工具类，这个工具的效率无需去管，反正发布之后也用不上
 */

@SuppressWarnings("unused")
public class Logger {

    private static boolean DEBUG = true;

    public static void d(Object tag, String message) {
        if (DEBUG) {
            Log.d(tag.getClass().getCanonicalName(), message);
        }
    }

    public static void e(Object tag, String message) {
        if (DEBUG) {
            Log.e(tag.getClass().getCanonicalName(), message);
        }
    }
}

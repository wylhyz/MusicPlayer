package io.lhyz.android.musicplayer.event;

import org.greenrobot.eventbus.EventBus;

public class MusicEventBus {

    static EventBus mEventBus = EventBus.getDefault();

    public static void post(MusicSelectedEvent event) {
        mEventBus.post(event);
    }

    public static void register(Object subscriber) {
        mEventBus.register(subscriber);
    }

    public static void unregister(Object subscriber) {
        mEventBus.unregister(subscriber);
    }
}

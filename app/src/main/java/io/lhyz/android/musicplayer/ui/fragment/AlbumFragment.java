package io.lhyz.android.musicplayer.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import io.lhyz.android.musicplayer.UAMP;
import io.lhyz.android.musicplayer.R;
import io.lhyz.android.musicplayer.bean.Album;
import io.lhyz.android.musicplayer.base.BaseFragment;
import io.lhyz.android.musicplayer.di.component.DaggerAlbumComponent;
import io.lhyz.android.musicplayer.di.module.AlbumModule;
import io.lhyz.android.musicplayer.presenter.AlbumPresenter;
import io.lhyz.android.musicplayer.ui.adapter.AlbumAdapter;
import io.lhyz.android.musicplayer.ui.adapter.AlbumLayoutManager;
import io.lhyz.android.musicplayer.ui.view.AlbumView;

/**
 * 通过专辑分类显示歌曲
 */
public class AlbumFragment extends BaseFragment implements AlbumView {
    @BindView(R.id.spl_list)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.rv_list)
    RecyclerView mRecyclerView;

    @Inject
    AlbumPresenter mPresenter;

    private AlbumAdapter mAdapter;

    public static AlbumFragment newInstance() {
        return new AlbumFragment();
    }

    @Override
    protected View createView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    protected void initInjector() {
        DaggerAlbumComponent.builder()
                .albumModule(new AlbumModule(0))
                .applicationComponent(((UAMP) getActivity().getApplication()).getApplicationComponent())
                .build()
                .inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSwipeRefreshLayout.setColorSchemeColors(R.color.colorPrimary, R.color.colorAccent, R.color.colorPrimaryDark);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.loadAllAlbum();
            }
        });
        mAdapter = new AlbumAdapter(getContext());
        mRecyclerView.setLayoutManager(new AlbumLayoutManager(context()));
        mRecyclerView.setAdapter(mAdapter);
        mPresenter.setView(this);
        if (savedInstanceState == null) {
            mPresenter.loadAllAlbum();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.destroy();
    }

    @Override
    public void showAllAlbum(List<Album> albumList) {
        mAdapter.updateAlbum(albumList);
    }

    @Override
    public void showLoading() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public Context context() {
        return getActivity();
    }
}

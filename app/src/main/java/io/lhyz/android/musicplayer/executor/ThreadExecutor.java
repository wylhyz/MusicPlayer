package io.lhyz.android.musicplayer.executor;

import java.util.concurrent.Executor;

/**
 * 非UI线程的执行器，这个实现应当是一个类似asyncTask的线程池应用，提供给
 * rxjava调用
 */
public interface ThreadExecutor extends Executor{
}

package io.lhyz.android.musicplayer.presenter;

public interface Presenter {

    void resume();

    void pause();

    void destroy();

}

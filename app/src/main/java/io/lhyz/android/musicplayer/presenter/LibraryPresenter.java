package io.lhyz.android.musicplayer.presenter;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import io.lhyz.android.musicplayer.bean.Music;
import io.lhyz.android.musicplayer.interactor.DefaultSubscriber;
import io.lhyz.android.musicplayer.interactor.UseCase;
import io.lhyz.android.musicplayer.di.annotation.AllMusic;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import io.lhyz.android.musicplayer.ui.view.LibraryView;

@PerActivity
public class LibraryPresenter implements Presenter {

    LibraryView mAllMusicView;

    UseCase mAllMusicUseCase;

    @Inject
    public LibraryPresenter(@AllMusic UseCase allMusicUseCase) {
        mAllMusicUseCase = allMusicUseCase;
    }


    public void setView(@NonNull LibraryView view) {
        mAllMusicView = view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        mAllMusicUseCase.unsubscribe();
        mAllMusicView = null;
    }

    public void loadAllMusic() {
        mAllMusicView.showLoading();
        getAllMusic();
    }

    private void getAllMusic() {
        mAllMusicUseCase.execute(new AllMusicSubscriber());
    }


    private final class AllMusicSubscriber extends DefaultSubscriber<List<Music>> {
        @Override
        public void onSuccess(List<Music> musics) {
            mAllMusicView.showAllMusic(musics);
            mAllMusicView.hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            mAllMusicView.hideLoading();
            mAllMusicView.showError(e.getMessage());
        }
    }
}

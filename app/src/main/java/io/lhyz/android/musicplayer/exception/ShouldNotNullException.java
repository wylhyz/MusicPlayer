package io.lhyz.android.musicplayer.exception;

/**
 * 这里使用运行时异常的原因是：一般Exception的子类都是在编译过程中使用的，这样的说法比较让人疑惑。
 * 我认为比较明确的说法是，exception是在代码中你想要捕获并处理的。
 * <p/>
 * 但是运行时异常虽然也是Exception的子类，单不同的是，它的使用是在运行时，也就是在java虚拟机上的异常，
 * 此类异常一般不能被捕获，直接抛出即可，这样的异常一般是对运行时某些因素的检查，如果有不符合预期的结果立刻终止操作
 */
@SuppressWarnings("unused")
public class ShouldNotNullException extends RuntimeException {
    public ShouldNotNullException() {
        super();
    }

    public ShouldNotNullException(String detailMessage) {
        super(detailMessage);
    }

    public ShouldNotNullException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ShouldNotNullException(Throwable throwable) {
        super(throwable);
    }
}

package io.lhyz.android.musicplayer.data;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.lhyz.android.musicplayer.bean.Album;
import io.lhyz.android.musicplayer.bean.Music;
import io.lhyz.android.musicplayer.exception.AlbumNotFoundException;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import rx.Observable;
import rx.Subscriber;

@PerActivity
public class AlbumProvider {

    private Context mContext;

    @Inject
    public AlbumProvider(Context context) {
        mContext = context;
    }

    public Observable<List<Album>> getAllAlbum() {

        String COUNT = "ALBUM_COUNT";
        Cursor cursor = mContext.getContentResolver()
                .query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new String[]{
                        "COUNT(*) AS " + COUNT,
                        MediaStore.Audio.Media.ALBUM_ID,
                        MediaStore.Audio.Media.ALBUM,
                        MediaStore.Audio.Media.ARTIST,
                        MediaStore.Audio.Media.DATA
                }, "0=0) GROUP BY (" + MediaStore.Audio.Media.ALBUM, null, null);

        final List<Album> albumList;
        if (cursor == null || cursor.getCount() == 0) {
            albumList = Collections.emptyList();
        } else {
            albumList = new ArrayList<>();
            Album album;
            while (cursor.moveToNext()) {
                String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                if (!path.toLowerCase().endsWith(".mp3")) {
                    continue;
                }
                int count = cursor.getInt(cursor.getColumnIndex(COUNT));
                long id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));
                String albumName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                album = new Album(id, artist, count, albumName);
                albumList.add(album);
            }
            cursor.close();
        }

        return Observable.create(new Observable.OnSubscribe<List<Album>>() {
            @Override
            public void call(Subscriber<? super List<Album>> subscriber) {
                if (albumList.size() != 0) {
                    subscriber.onNext(albumList);
                    subscriber.onCompleted();
                    return;
                }
                subscriber.onError(new AlbumNotFoundException());
            }
        });
    }

    public Observable<List<Music>> getAlbum(final long albumId) {
        Cursor cursor = mContext.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[]{
                        MediaStore.Audio.Media._ID,
                        MediaStore.Audio.Media.TITLE,
                        MediaStore.Audio.Media.ARTIST,
                        MediaStore.Audio.Media.DATA,
                }, MediaStore.Audio.Media.ALBUM_ID + " = ?", new String[]{"" + albumId}, null);

        final List<Music> musicList;
        if (cursor == null || cursor.getCount() == 0) {
            musicList = Collections.emptyList();
        } else {


            musicList = new ArrayList<>();
            Music music;
            while (cursor.moveToNext()) {
                long id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media._ID));
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                music = new Music(id, title, artist, path);
                musicList.add(music);
            }
            cursor.close();
        }

        return Observable.create(new Observable.OnSubscribe<List<Music>>() {
            @Override
            public void call(Subscriber<? super List<Music>> subscriber) {
                if (musicList.size() != 0) {
                    subscriber.onNext(musicList);
                    subscriber.onCompleted();
                    return;
                }
                subscriber.onError(new AlbumNotFoundException());
            }
        });
    }
}

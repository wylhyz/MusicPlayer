package io.lhyz.android.musicplayer.ui.fragment;

import io.lhyz.android.musicplayer.base.BaseFragment;

public abstract class BaseNowFragment extends BaseFragment {
    public static final String EXTRA_MUSIC_ID = "EXTRA_MUSIC_ID";
}

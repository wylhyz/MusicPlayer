package io.lhyz.android.musicplayer.di.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import io.lhyz.android.musicplayer.UAMP;
import io.lhyz.android.musicplayer.executor.PostExecutionThread;
import io.lhyz.android.musicplayer.executor.ThreadExecutor;
import io.lhyz.android.musicplayer.di.annotation.PerApplication;
import io.lhyz.android.musicplayer.executor.JobExecutor;
import io.lhyz.android.musicplayer.executor.UIThread;
import io.lhyz.android.musicplayer.manager.ConfigureManager;

@Module
public class ApplicationModule {
    private final UAMP mApplication;

    public ApplicationModule(UAMP application) {
        mApplication = application;
    }

    @Provides
    @PerApplication
    Context provideApplicationContext() {
        return mApplication;
    }

    @Provides
    @PerApplication
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @PerApplication
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

    @Provides
    @PerApplication
    ConfigureManager provideConfigureManager() {
        return new ConfigureManager(mApplication);
    }

}

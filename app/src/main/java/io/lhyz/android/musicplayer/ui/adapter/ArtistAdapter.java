package io.lhyz.android.musicplayer.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.lhyz.android.musicplayer.R;
import io.lhyz.android.musicplayer.bean.Artist;

public class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.ArtistViewHolder> {

    List<Artist> mData;

    public ArtistAdapter() {
        mData = new ArrayList<>(10);
    }

    public void updateArtist(List<Artist> artistList) {
        if (mData.size() != 0) {
            mData.clear();
        }
        mData.addAll(artistList);
        notifyDataSetChanged();
    }

    static final class ArtistViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.artist)
        TextView tvArtist;
        @BindView(R.id.song_count)
        TextView tvCount;

        public ArtistViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ArtistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ArtistViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_artist, parent, false));
    }

    @Override
    public void onBindViewHolder(final ArtistViewHolder holder, int position) {
        holder.tvArtist.setText(mData.get(position).getName());
        holder.tvCount.setText(mData.get(position).getCount() + " 首歌曲");
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }
}

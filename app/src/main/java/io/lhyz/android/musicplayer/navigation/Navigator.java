package io.lhyz.android.musicplayer.navigation;

import android.app.Activity;
import android.content.Intent;

import io.lhyz.android.musicplayer.R;
import io.lhyz.android.musicplayer.ui.activity.NowPlayingActivity;
import io.lhyz.android.musicplayer.ui.activity.SettingActivity;

/**
 * app内部导航类
 * <p/>
 * 加入了简单的切换动画(由于mainActivity使用了singleTask启动模式，所以不适合再用当前的切换动画，暂时放弃)
 */
public final class Navigator {

    private Navigator() {
    }

    public static void navigateToSettingActivity(Activity activity) {
        activity.startActivity(new Intent(activity, SettingActivity.class));
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public static void navigateToNowPlayingActivity(Activity activity) {
        activity.startActivity(new Intent(activity, NowPlayingActivity.class));
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}

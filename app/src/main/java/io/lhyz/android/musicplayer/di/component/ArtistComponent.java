package io.lhyz.android.musicplayer.di.component;

import dagger.Component;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import io.lhyz.android.musicplayer.di.module.ArtistModule;
import io.lhyz.android.musicplayer.ui.fragment.ArtistFragment;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ArtistModule.class)
public interface ArtistComponent {
    void inject(ArtistFragment fragment);
}

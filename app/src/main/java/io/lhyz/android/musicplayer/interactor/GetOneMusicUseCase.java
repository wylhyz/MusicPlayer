package io.lhyz.android.musicplayer.interactor;

import javax.inject.Inject;

import io.lhyz.android.musicplayer.executor.PostExecutionThread;
import io.lhyz.android.musicplayer.executor.ThreadExecutor;
import io.lhyz.android.musicplayer.data.MusicProvider;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import rx.Observable;

@PerActivity
public class GetOneMusicUseCase extends UseCase {

    private final long musicId;
    MusicProvider mMusicProvider;

    @Inject
    public GetOneMusicUseCase(final long musicId, MusicProvider musicProvider,
                              PostExecutionThread postExecutionThread,
                              ThreadExecutor threadExecutor) {
        super(postExecutionThread, threadExecutor);

        this.musicId = musicId;
        mMusicProvider = musicProvider;
    }

    @Override
    protected Observable buildObservable() {
        return mMusicProvider.getMusic(musicId);
    }
}

package io.lhyz.android.musicplayer.service;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import java.util.HashMap;
import java.util.Map;

import io.lhyz.android.musicplayer.IMusicPlaybackManager;

/**
 * 控制music播放服务的类，为完全的静态类
 */
public final class MusicPlayback {
    static IMusicPlaybackManager sManager = null;//此管理器用来提供一些通用的操作，不通用的操作可以通过外部connection来获取binder来使用
    static Map<ContextWrapper, ServiceBinder> sBinderMap = new HashMap<>();

    public static ServiceToken bindService(Activity context) {
        return bindService(context, null);
    }

    public static ServiceToken bindService(Activity context, ServiceConnection callback) {
        Activity activity = context.getParent();
        //获取实际的activity（最外部顶级activity）
        if (activity == null) {
            activity = context;
        }
        ContextWrapper cw = new ContextWrapper(activity);
        //用activity启动服务（一旦启动完成，service再被启动的时候将不会调用onCreate）
//        cw.startService(new Intent(cw, MusicPlayerService.class));//所以这句实际上是尝试启动没有启动的服务
        //如果服务已经启动，会调用onStartCommand方法
        ServiceBinder sb = new ServiceBinder(callback);//将外部连接托管给此类
        if (cw.bindService(new Intent().setClass(cw, MusicPlayerService.class), sb, 0)) {
            //尝试绑定播放服务，并加入到绑定表中
            sBinderMap.put(cw, sb);
            return new ServiceToken(cw);
        }
        //到此表示绑定失败
        return null;
    }

    //解除绑定
    public static void unbindService(ServiceToken token) {
        if (token == null) {
            //一般不可能到这里
            return;
        }
        ContextWrapper cw = token.mContextWrapper;
        ServiceBinder sb = sBinderMap.remove(cw);
        if (sb == null) {
            //没有绑定的token就没办法解绑
            return;
        }
        cw.unbindService(sb);
        if (sBinderMap.isEmpty()) {
            //停止服务
//            cw.stopService(new Intent(cw, MusicPlayerService.class));
        }
    }

    /**
     * 令牌，cw对象可以用来启动和绑定service（每个与播放service绑定的activity都有一个令牌，如果在fragment或其他里面使用，
     * 也可以通过getActivity来获取一个Token）
     */
    public static class ServiceToken {
        ContextWrapper mContextWrapper;

        public ServiceToken(ContextWrapper contextWrapper) {
            mContextWrapper = contextWrapper;
        }
    }

    /**
     * 托管外部连接监听（实际上就是一个对外部connection对象的简单包装器）
     */
    public static class ServiceBinder implements ServiceConnection {

        private ServiceConnection mCallback;

        public ServiceBinder(ServiceConnection callback) {
            mCallback = callback;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            sManager = IMusicPlaybackManager.Stub.asInterface(service);//获取服务

            if (mCallback != null) {
                mCallback.onServiceConnected(name, service);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            if (mCallback != null) {
                mCallback.onServiceDisconnected(name);
            }
        }
    }

    static Application mApplication;

    public static void startService(Application application) {
        mApplication = application;
        mApplication.startService(new Intent(mApplication, MusicPlayerService.class));
    }

    public static void stopService() {
        if (mApplication != null) {
            mApplication.stopService(new Intent(mApplication, MusicPlayerService.class));
        }
    }
}

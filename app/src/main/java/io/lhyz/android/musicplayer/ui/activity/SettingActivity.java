package io.lhyz.android.musicplayer.ui.activity;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import javax.inject.Inject;

import butterknife.BindView;
import io.lhyz.android.musicplayer.UAMP;
import io.lhyz.android.musicplayer.R;
import io.lhyz.android.musicplayer.base.BaseActivity;
import io.lhyz.android.musicplayer.manager.ConfigureManager;

import static com.google.common.base.Preconditions.checkNotNull;

public class SettingActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    public static class SettingFragment extends PreferenceFragment {

        @Inject
        ConfigureManager mConfigureManager;

        public SettingFragment() {
            setRetainInstance(true);
        }

        public static SettingFragment newInstance() {
            return new SettingFragment();
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            initInjector();
            addPreferencesFromResource(R.xml.settings);
        }

        private void initInjector() {
            ((UAMP) getActivity().getApplication()).getApplicationComponent().inject(this);
        }

        @Override
        public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {

            if ("pref_clean".equals(preference.getKey())) {
                //pass
                //TODO:clean the cache
            } else if ("pref_night_mode".equals(preference.getKey())) {
                boolean isNightMode = preference.getSharedPreferences().getBoolean("pref_night_mode", false);//默认的情况是false
                mConfigureManager.setNightMode(isNightMode);//将结果保存
                UAMP.switchNightMode(isNightMode);
            }
            return true;
        }
    }

    @Override
    protected void initInjector() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initVariables() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        checkNotNull(actionBar);
        actionBar.setTitle(R.string.activity_title_settings);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        getFragmentManager().beginTransaction()
                .add(R.id.setting_container, SettingFragment.newInstance())
                .commit();

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_settings;
    }
}

package io.lhyz.android.musicplayer.manager;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

import io.lhyz.android.musicplayer.di.annotation.PerApplication;

@PerApplication
public class ConfigureManager {

    private static final String CONFIG_FILE_NAME = "musicplayer_config";

    private static final String KEY_NIGHT_MODE = "KEY_NIGHT_MODE";//夜间模式
    private static final String KEY_ACTIVE_TAB = "KEY_ACTIVE_TAB";//激活的首页页面
    private static final String KEY_ACTIVE_NOW_THEME = "KEY_ACTIVE_NOW_THEME";//nowplaying播放界面
    private static final String KEY_NOW_PLAYING = "KEY_NOW_PLAYING";//正在播放的歌曲id

    public static final int TAB_LIBRARY = 0;
    public static final int TAB_ALBUM = 1;
    public static final int TAB_ARTIST = 2;

    public static final int NOW_THEME_1 = 0;
    public static final int NOW_THEME_2 = 1;

    SharedPreferences mSharedPreferences;

    @Inject
    public ConfigureManager(Context context) {
        mSharedPreferences = context.getSharedPreferences(CONFIG_FILE_NAME, Context.MODE_PRIVATE);
    }

    public boolean isNightMode() {
        return mSharedPreferences.getBoolean(KEY_NIGHT_MODE, false);
    }

    public void setNightMode(boolean mode) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(KEY_NIGHT_MODE, mode);
        editor.apply();
    }

    public int getActiveTab() {
        return mSharedPreferences.getInt(KEY_ACTIVE_TAB, TAB_LIBRARY);
    }

    public void setActiveTab(int type) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(KEY_ACTIVE_TAB, type);
        editor.apply();
    }

    public int getNowTheme() {
        return mSharedPreferences.getInt(KEY_ACTIVE_NOW_THEME, NOW_THEME_1);
    }

    public void setNowTheme(int type) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(KEY_ACTIVE_NOW_THEME, type);
        editor.apply();
    }

    public void setNowPlaying(long musicId) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putLong(KEY_NOW_PLAYING, musicId);
        editor.apply();
    }

    public long getNowPlaying() {
        return mSharedPreferences.getLong(KEY_NOW_PLAYING, -1);
    }
}

package io.lhyz.android.musicplayer.interactor;

import io.lhyz.android.musicplayer.executor.PostExecutionThread;
import io.lhyz.android.musicplayer.executor.ThreadExecutor;
import io.lhyz.android.musicplayer.data.AlbumProvider;
import rx.Observable;

public class GetOneAlbumUseCase extends UseCase {

    private AlbumProvider mAlbumProvider;
    private long albumId;

    public GetOneAlbumUseCase(final long albumId,
                              AlbumProvider albumProvider,
                              PostExecutionThread postExecutionThread,
                              ThreadExecutor threadExecutor) {
        super(postExecutionThread, threadExecutor);

        mAlbumProvider = albumProvider;
        this.albumId = albumId;

    }

    @Override
    protected Observable buildObservable() {
        return mAlbumProvider.getAlbum(albumId);
    }
}

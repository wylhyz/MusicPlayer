package io.lhyz.android.musicplayer.data;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.lhyz.android.musicplayer.bean.Artist;
import io.lhyz.android.musicplayer.bean.Music;
import io.lhyz.android.musicplayer.exception.ArtistNotFoundException;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import rx.Observable;
import rx.Subscriber;

@PerActivity
public class ArtistProvider {

    private Context mContext;

    @Inject
    public ArtistProvider(Context context) {
        mContext = context;
    }

    public Observable<List<Artist>> getAllArtist() {
        String COUNT = "MUSIC_COUNT";
        Cursor cursor = mContext.getContentResolver()
                .query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new String[]{
                        "COUNT(*) AS " + COUNT,
                        MediaStore.Audio.Media.ARTIST_ID,
                        MediaStore.Audio.Media.ARTIST,
                        MediaStore.Audio.Media.DATA
                }, "0=0) GROUP BY (" + MediaStore.Audio.Media.ARTIST, null, null);

        final List<Artist> artistList;
        if (cursor == null || cursor.getCount() == 0) {
            artistList = Collections.emptyList();
        } else {
            artistList = new ArrayList<>();
            Artist artist;
            while (cursor.moveToNext()) {
                if (!cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA)).toLowerCase().endsWith(".mp3")) {
                    continue;
                }
                long id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST_ID));
                String artistName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                int count = cursor.getInt(cursor.getColumnIndex(COUNT));
                artist = new Artist(id, artistName, count);
                artistList.add(artist);
            }
            cursor.close();
        }

        return Observable.create(new Observable.OnSubscribe<List<Artist>>() {
            @Override
            public void call(Subscriber<? super List<Artist>> subscriber) {
                if (artistList.size() != 0) {
                    subscriber.onNext(artistList);
                    subscriber.onCompleted();
                    return;
                }
                subscriber.onError(new ArtistNotFoundException());
            }
        });
    }

    //TODO
    public Observable<List<Music>> getArtist(final long artistId) {
        Cursor cursor = mContext.getContentResolver()
                .query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                        new String[]{MediaStore.Audio.Media._ID,
                                MediaStore.Audio.Media.TITLE,
                                MediaStore.Audio.Media.ARTIST,
                                MediaStore.Audio.Media.DATA},
                        MediaStore.Audio.Media.ARTIST_ID + " = ?",
                        new String[]{"" + artistId}, null);
        final List<Music> musicList;
        if (cursor == null || cursor.getCount() == 0) {
            musicList = Collections.emptyList();
        } else {
            musicList = new ArrayList<>();
            Music music;
            while (cursor.moveToNext()) {
                long id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media._ID));
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                music = new Music(id, title, artist, path);
                musicList.add(music);
            }
            cursor.close();
        }

        return Observable.create(new Observable.OnSubscribe<List<Music>>() {
            @Override
            public void call(Subscriber<? super List<Music>> subscriber) {
                if (musicList.size() != 0) {
                    subscriber.onNext(musicList);
                    subscriber.onCompleted();
                    return;
                }
                subscriber.onError(new ArtistNotFoundException());
            }
        });
    }
}

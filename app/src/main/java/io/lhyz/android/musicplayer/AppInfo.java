package io.lhyz.android.musicplayer;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;

public class AppInfo {

    private static Context sContext = UAMP.getInstance();

    private static final String BASE_DIR = Environment.getExternalStorageDirectory().getAbsolutePath();
    private static final String CACHE_DIR = BASE_DIR + "/io.lhyz.android.musicplayer/cache";
    public static final String BITMAP_CACHE_DIR = CACHE_DIR + "/bitmaps";


    /**
     * 应用包名
     */
    public static String getPackageName() {
        return sContext.getPackageName();
    }

    /**
     * 版本号,这个是用来检查更新的（如果服务器获取的版本号大于这个版本号，就提示更新，不过暂时没有服务器资源，也只能看着解解闷了）
     */
    public static int getVersionCode() {
        try {
            PackageManager packageManager = sContext.getPackageManager();
            PackageInfo info = packageManager.getPackageInfo(sContext.getPackageName(), 0);
            return info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * 获取应用的版本名称
     */
    public static String getVersionName() {
        try {
            PackageManager manager = sContext.getPackageManager();
            PackageInfo info = manager.getPackageInfo(sContext.getPackageName(), 0);
            return info.versionName;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取应用的名称
     */
    public static String getAppName() {
        PackageManager packageManager = null;
        ApplicationInfo applicationInfo = null;
        try {
            packageManager = sContext.getPackageManager();
            applicationInfo = packageManager.getApplicationInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            applicationInfo = null;
        }
        return (String) packageManager.getApplicationLabel(applicationInfo);
    }
}

package io.lhyz.android.musicplayer;

import android.app.Application;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatDelegate;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import javax.inject.Inject;

import io.lhyz.android.musicplayer.di.component.ApplicationComponent;
import io.lhyz.android.musicplayer.di.component.DaggerApplicationComponent;
import io.lhyz.android.musicplayer.di.module.ApplicationModule;
import io.lhyz.android.musicplayer.manager.ConfigureManager;
import io.lhyz.android.musicplayer.service.MusicPlayback;

public class UAMP extends Application {

    ApplicationComponent mComponent;

    @Inject
    ConfigureManager mPreferManager;

    private static UAMP sMusicApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        sMusicApplication = this;

        mComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        mComponent.inject(this);

        switchNightMode(mPreferManager.isNightMode());

        MusicPlayback.startService(sMusicApplication);
    }

    public static UAMP getInstance() {
        return sMusicApplication;
    }

    public ApplicationComponent getApplicationComponent() {
        return mComponent;
    }


    /**
     * 提供一个全局切换模式的接口
     *
     * @param flag 标志位，设置为true表示立即开始夜间模式,false为开启自动模式
     */
    public static void switchNightMode(boolean flag) {
        if (flag) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //横屏之后清掉缓存和文件缓存（其实也可以在ImageLoader中设置两种缓存和两种文件缓存，不过这么想觉得很麻烦）

    }


    @Override
    public void onTerminate() {
        MusicPlayback.startService(sMusicApplication);
        super.onTerminate();
    }
}

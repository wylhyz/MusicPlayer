package io.lhyz.android.musicplayer.exception;

public class MusicNotFoundExecption extends RuntimeException {

    public MusicNotFoundExecption() {
        super();
    }

    public MusicNotFoundExecption(String detailMessage) {
        super(detailMessage);
    }

    public MusicNotFoundExecption(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public MusicNotFoundExecption(Throwable throwable) {
        super(throwable);
    }
}

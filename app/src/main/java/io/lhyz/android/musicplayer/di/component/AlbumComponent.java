package io.lhyz.android.musicplayer.di.component;

import dagger.Component;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import io.lhyz.android.musicplayer.di.module.AlbumModule;
import io.lhyz.android.musicplayer.ui.fragment.AlbumFragment;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = AlbumModule.class)
public interface AlbumComponent {
    void inject(AlbumFragment fragment);
}

package io.lhyz.android.musicplayer.data;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.lhyz.android.musicplayer.bean.Music;
import io.lhyz.android.musicplayer.exception.MusicNotFoundExecption;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import rx.Observable;
import rx.Subscriber;

/**
 * 加载音乐数据（查询的是音乐列表和单个音乐的数据）
 */
@PerActivity
public class MusicProvider {

    private Context mContext;

    @Inject
    public MusicProvider(Context context) {
        mContext = context;
    }

    public Observable<List<Music>> getAllMusic() {
        Cursor cursor = mContext.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new String[]
                        {
                                MediaStore.Audio.Media._ID,
                                MediaStore.Audio.Media.TITLE,
                                MediaStore.Audio.Media.ARTIST,
                                MediaStore.Audio.Media.DATA,
                        }, null, null, null, null);
        final List<Music> musics;

        if (cursor == null || cursor.getCount() == 0) {
            musics = Collections.emptyList();
        } else {
            musics = new ArrayList<>();
            Music music;
            while (cursor.moveToNext()) {
                String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                if (path.toLowerCase().endsWith(".mp3")) {
                    long id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media._ID));
                    String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                    String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                    music = new Music(id, title, artist, path);
                    musics.add(music);
                }
            }
            cursor.close();
        }

        return Observable.create(new Observable.OnSubscribe<List<Music>>() {
            @Override
            public void call(Subscriber<? super List<Music>> subscriber) {
                if (musics.size() != 0) {
                    subscriber.onNext(musics);
                    subscriber.onCompleted();
                    return;
                }
                subscriber.onError(new MusicNotFoundExecption());
            }
        });
    }

    public Observable<Music> getMusic(final long musicId) {
        Cursor cursor = mContext.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new String[]
                        {
                                MediaStore.Audio.Media._ID,
                                MediaStore.Audio.Media.TITLE,
                                MediaStore.Audio.Media.ARTIST,
                                MediaStore.Audio.Media.DATA,
                        }, MediaStore.Audio.Media._ID + " = ?", new String[]{"" + musicId}, null, null);

        final Music music;
        if (cursor == null || cursor.getCount() == 0) {
            music = null;
        } else {
            cursor.moveToFirst();
            long id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media._ID));
            String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
            String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
            String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
            music = new Music(id, title, artist, path);
            cursor.close();
        }

        return Observable.create(new Observable.OnSubscribe<Music>() {
            @Override
            public void call(Subscriber<? super Music> subscriber) {
                if (music != null) {
                    subscriber.onNext(music);
                    subscriber.onCompleted();
                }
                subscriber.onError(new MusicNotFoundExecption());
            }
        });
    }
}

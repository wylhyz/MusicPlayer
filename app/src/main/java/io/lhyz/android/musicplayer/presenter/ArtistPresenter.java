package io.lhyz.android.musicplayer.presenter;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import io.lhyz.android.musicplayer.bean.Artist;
import io.lhyz.android.musicplayer.interactor.DefaultSubscriber;
import io.lhyz.android.musicplayer.interactor.UseCase;
import io.lhyz.android.musicplayer.di.annotation.AllArtist;
import io.lhyz.android.musicplayer.di.annotation.PerActivity;
import io.lhyz.android.musicplayer.ui.view.ArtistView;

@PerActivity
public class ArtistPresenter implements Presenter {

    ArtistView mArtistView;

    UseCase mGetAllArtistUseCase;

    @Inject
    public ArtistPresenter(@AllArtist UseCase getAllArtistUseCase) {
        mGetAllArtistUseCase = getAllArtistUseCase;
    }


    public void setView(@NonNull  ArtistView artistView) {
        mArtistView = artistView;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        mGetAllArtistUseCase.unsubscribe();
        mArtistView = null;
    }


    public void loadAllArtist() {
        mArtistView.showLoading();
        getAllArtist();
    }

    public void getAllArtist() {
        mGetAllArtistUseCase.execute(new AllArtistSubscriber());
    }

    private final class AllArtistSubscriber extends DefaultSubscriber<List<Artist>> {
        @Override
        public void onSuccess(List<Artist> artistList) {
            mArtistView.showAllArtist(artistList);
            mArtistView.hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            mArtistView.hideLoading();
            mArtistView.showError(e.getMessage());
        }
    }
}

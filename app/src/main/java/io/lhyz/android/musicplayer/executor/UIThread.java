package io.lhyz.android.musicplayer.executor;

import javax.inject.Inject;

import io.lhyz.android.musicplayer.di.annotation.PerApplication;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;

/**
 * 主线程
 */
@PerApplication
public class UIThread implements PostExecutionThread {

    @Inject
    public UIThread() {
    }

    @Override
    public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }
}

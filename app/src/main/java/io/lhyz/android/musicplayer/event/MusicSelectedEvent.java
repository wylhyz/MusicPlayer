package io.lhyz.android.musicplayer.event;

import io.lhyz.android.musicplayer.bean.Music;

/**
 * music被选中的事件,由于我在配置文件中保存了上次退出应用之后最后播放的musicId，
 * <p/>
 * 所以重开的时候会有一个恢复操作，恢复操作的时候无不希望重新准备音源，因此，在这里通过事件来区分
 */
public class MusicSelectedEvent {

    public static final int MARK_TO_PLAYING = 0x1;
    public static final int MARK_TO_DISPLAY = 0x2;

    public final Music music;
    public final int mark;

    public MusicSelectedEvent(Music music) {
        this(music, MARK_TO_DISPLAY);//默认就是显示状态，只有在点击事件明确的状态下才能使用PLAYING
    }

    public MusicSelectedEvent(Music music, int mark) {
        this.music = music;
        this.mark = mark;
    }
}

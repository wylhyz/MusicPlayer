package io.lhyz.android.musicplayer.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import io.lhyz.android.musicplayer.exoplayer.MusicPlayer;
import io.lhyz.android.musicplayer.IMusicPlaybackManager;

/**
 * 此服务的解释：
 * <p/>
 * 要想service支持被组件绑定，必须提供onBind方法，然后可以通过在组件中调用bindService的方法来实现与服务的交互。
 * <p/>
 * 如果不想再与service绑定，可以通过unbindService方法来实现解绑
 * <p/>
 * 如果一个服务没有被绑定，同时也不是通过startService（想要使用这个就必须提供onStartCommand方法）启动的话（这样会无限运行，知道stopSelf或stopService被显示调用）
 * <p/>
 * 也就是说上述两种启动服务的方法的区别是，一个start是运行长时间任务的首选，比如下载等长时间任务，与组件的交互比较少甚至没有
 * <p/>
 * 另一种bind是组件需要和服务交互的，比如像我这里实现的音乐播放，需要实时与前台界面的操作同步
 */
public class MusicPlayerService extends Service {

    public static final String EXTRA_MUSIC = "io.lhyz.android.musicplayer.service.EXTRA_MUSIC";

    private MusicPlayer mMusicPlayer;

    //这个binder实现的方法都是将来暴露给外部控制器的
    private Binder mBinder = new IMusicPlaybackManager.Stub() {

        @Override
        public void prepare(String path) throws RemoteException {
            mMusicPlayer.prepare(path);
        }

        @Override
        public void play() throws RemoteException {
            mMusicPlayer.play();
        }

        @Override
        public void pause() throws RemoteException {
            mMusicPlayer.pause();
        }

        @Override
        public void stop() throws RemoteException {
            mMusicPlayer.stop();
        }

        @Override
        public boolean isPlaying() throws RemoteException {
            return mMusicPlayer.isPlaying();
        }

        @Override
        public long getDuration() throws RemoteException {
            return mMusicPlayer.getDuration();
        }

        @Override
        public long getPosition() throws RemoteException {
            return mMusicPlayer.getPosition();
        }

        @Override
        public String getPath() throws RemoteException {
            return mMusicPlayer.getPath();
        }
    };

    public MusicPlayerService() {
    }

    /**
     * 只在服务创建的时候被调用，服务存在情况下不会调用
     */
    @Override
    public void onCreate() {
        super.onCreate();
        //服务被创建的时候，同时创建播放器来绑定给服务
        mMusicPlayer = new MusicPlayer(this);
    }

    /**
     * 只在服务被销毁的时候被调用
     */
    @Override
    public void onDestroy() {
        //服务被销毁的时候销毁播放器(如果服务被销毁的话，外部将无法向此服务发送消息)
        if (mMusicPlayer != null) {
            mMusicPlayer.release();
            mMusicPlayer = null;
        }
        super.onDestroy();
    }

    /**
     * 没有提供onStartCommand的事实是我只想通过绑定来与service交互
     *
     * @param intent nothing
     * @return 一个用来与外部交互的binder对象
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
}

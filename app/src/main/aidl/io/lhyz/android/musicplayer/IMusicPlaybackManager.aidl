package io.lhyz.android.musicplayer;

import java.lang.String;
/**
**/
interface IMusicPlaybackManager {
    void prepare(String path);
    void play();
    void pause();
    void stop();

    boolean isPlaying();
    long getDuration();
    long getPosition();
    String getPath();
}

#Universal Android Music Player Sample 精简版

![持续集成](https://travis-ci.org/wylhyz/MusicPlayer.svg?branch=master)

目的：由于原来的项目实际上支持的范围太广，一般的情况下我们不需要考虑那么多的设备，同时由于项目使用了gcm等服务，国内实际上没有多少手机可以用。所以
本着学习的目的，将整个项目拆解，按照自己的想法编写代码，UI遵照原应用的手机版本，实现代码会主要从原项目中摘录，以作学习之用。

原项目Github地址 ： [android-UniversalMusicPlayer](https://github.com/googlesamples/android-UniversalMusicPlayer)


## Update

__*2016-03-15*__ 更新了所有Util类，所有代码完全拷贝原项目,另外REMOTE JSON的数据保存在了assets/music.json下

__*2016-05-29*__ 源文件加入了许可证信息，同时加入了未来想用的依赖库，准备转换为本地播放器

__*2016-05-29*__ 由于原项目的UI和某些实现比较不符合我的预想（我选择使用Clean架构和MVP来尝试编码），所以删除了所有相关文件，准备重写架构

__*2016-05-30*__ 由于对于dagger2不熟悉，同时使用butterknife有的一些小问题，所以暂时删除了这两个依赖，实现了简单的第一屏音乐列表

__*2016-05-31*__ 对于在定义的的activity基类中管理fragment的问题导致presenter在旋转变换后被置空的问题，简单删除了相关代码，直接在当前activity中硬编码管理addFragment，
同时加入了与playback control这一fragment的消息传递和对listFragment列表界面的选中事件监听，下一步准备加入对ExoPlayer的调用。

__*2016-06-02*__ 通过了解ExoPlayer的音频功能，在项目中加入了音乐播放服务。 

__*2016-06-03*__ 添加了通知栏和全屏播放界面，由于多fragment因编码管理复杂。

__*2016-06-04*__ 大幅度重构代码，添加greenDao数据库支持，移除原来的realm，同时准备再次启用dagger2

__*2016-06-05*__ 
- 完成基本drawer布局和所有歌曲fragment界面的mvp化，同时参照Clean架构完成了依赖注入框架模板。目前完成的模板是LibraryFragment及其相关mvp类的依赖注入模型
- 参照Timber开源音乐播放器的某些UI设计，准备首页使用三种fragment进行替换分类型显示，然后正在播放界面和设置界面使用activity，同时关于界面使用简单的Dialog
下一步准备完成设置界面以及设置数据持久化存储的相关管理

__*2016-06-10*__ 完善自建的简单ImageLoader逻辑和多线程问题

__*2016-06-12*__ 为QuickControl fragment 添加了与服务交互的功能，基本实现简单的播放选中，暂停播放功能

__*2016-07-04*__ 熬过考试半个月，更新了一个ImageLoader，同时消除了recycler view中复用图片的错位问题


## bug
- [x] <del>无法从文件中解析artist的信息（这个信息指歌手图片文件）我看了一下Timber和其他实现，大部分是用的网络开源api获取的</del>

- [x] 多尺寸图片加载问题-在quick control界面的图片和list的图片大小显示会相同

- [ ] 关于fragment的重影问题和activity如何管理多fragment问题还有待解决 

## issue
- [x] 在设置界面，必须退出设置界面之后才能看到设置的夜间模式，因为要想使其当前界面生效，会导致界面闪烁和不必要的代码，因此决定放弃在设置界面同步夜间模式。

- [x] 当前虽然模板代码已经完成，不过用起来总觉得重复代码太多，我今后看看能不能再精简一下（第一个版本出来之前大概不会再大规模重构了）

- [x] 当前ImageLoader还是很多问题，原因就是我使用了多线程和太多的静态变量

- [x] 由于在网络api加载图片的复杂性，而且不论是用自己写的ImageLoader还是开源库，获取图片的效果都不是很好，同时我使用的LastFM接口访问很可能出问题，所以就放弃了
 - 我发现主要问题可能就是因为使用retrofit的时候如果再使用Picasso或者是Glide的时候在Success的时候还是可能串图（显示位置出问题）

- [x] 在转屏的时候，专辑界面的图片不会缩放，也就是说直接从内存中读取的数据比当前iv小，所以我修改了application配置，在转屏的时候清掉缓存数据

- [x] 根据Music完成了简单的服务绑定问题，通过在application 中 startService来构造一个长期服务，同时在fragment中可以通过外部activity绑定service并在销毁时解除绑定（在applicaiton销毁的时候stop服务）

- [x] 因为在屏幕旋转的时候出问题太多，所以已经设置了屏幕为竖屏模式

- [x] 因为暂时用不到数据库，所以删除了GreenDao的依赖，同时将最先需要的Music bean类进行了序列化重写，来实现服务的支持（此序列化是通过直接实现Parcelable接口来完成的）

- [x] 使用EventBus来代替监听器在多个组件（先实现了在两个fragment之间）传递消息

## TODO
- [x] dagger2的使用

- [x] 从音乐中解码出唱片封面

- [ ] *加入播放控制和列表功能*

- [x] 完善fragment的管理

- [x] 完成原代码多module重构

- [x] 完成夜间模式的调试,现在可以在设置界面进行切换

- [x] 加入了简单的关于界面，显示当前软件的版本信息和作者信息

- [x] 
 - 优化首页bitmap显示，自己实现了一个bitmap本地loader，加入了线程池和二级缓存，同时解决了一个以前没想过的多线程变量同步问题（具体可以看看core下ImageLoader文件，里面有详细说明）
 - 由于图片大小不确定，所以在某些实现上偷了点懒

- [x] 使用DiskLruCache改写ImageLoader来进行文件缓存（DiskLruCache比较难用，大量的try/catch写的我心累）

- [x] 使用了[Travis-CI](https://travis-ci.org/)做持续集成（还不错，除了最开始测试的时候时间较长）

- [ ] 在nowPlaying界面实现精准控制，同时添加对progress的更新


## art
api19  | api21
-------|--------
![](/art/device-2016-06-12-232008.png) | ![](/art/device-2016-06-09-002123.png)
![](/art/device-2016-06-12-232037.png) | ![](/art/device-2016-06-05-230157.png)
![](/art/device-2016-06-06-225620.png) | ![](/art/device-2016-06-09-002306.png)

License
========

原项目

>Copyright (C) 2014 The Android Open Source Project
 
>Licensed under the Apache License, Version 2.0 (the "License");
>you may not use this file except in compliance with the License.
>You may obtain a copy of the License at
 
>   http://www.apache.org/licenses/LICENSE-2.0
 
>Unless required by applicable law or agreed to in writing, software
>distributed under the License is distributed on an "AS IS" BASIS,
>WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>See the License for the specific language governing permissions and
>limitations under the License.

本项目

>Copyright 2016 lhyz Android Open Source Project

>This program is free software: you can redistribute it and/or modify
>it under the terms of the GNU General Public License as published by
>the Free Software Foundation, either version 3 of the License, or
>(at your option) any later version.

>This program is distributed in the hope that it will be useful,
>but WITHOUT ANY WARRANTY; without even the implied warranty of
>MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
>GNU General Public License for more details.

>You should have received a copy of the GNU General Public License
>along with this program.  If not, see <http://www.gnu.org/licenses/>.
